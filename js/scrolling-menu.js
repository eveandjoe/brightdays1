(function($) { 
"use strict";

    function scrollToHref(event) {
        const $anchor = $(this);
        $('html, body').animate({scrollTop: $($anchor.attr('href')).offset().top - 55}, 'slow');
        event.preventDefault();
    };

    $(document).on('click', 'a.nav-link', scrollToHref)
    $(document).on('click', 'a.nav-button', scrollToHref)
    $(document).on('click', '.back-to-top', scrollToHref)

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById(".back-to-top").style.display = "block";
        } else {
            document.getElementById(".back-to-top").style.display = "none";
        }
    }

    window.onscroll = function() {scrollFunction()};
}(jQuery));

$(".nav .nav-link").on("click", function(){
    $(".nav").find(".active").removeClass("active");
    $(this).addClass("active");
});